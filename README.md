# OauthTest

A project to demonstrate how to use Oauth with the Angular 2 framework. This is a sample. It is up to you to adapt to your situation.

## Development & production

```
npm install
ng serve
ng build --prod
```
## Content

0. AcceptTokenComponent :
    - path : /accepttoken
    - description : a component to handle the response of the Emalsys Oauth Server

1. Login Component :
    - path : /login
    - description : An example to show how to add the oauth logic


2. Home Component :
    - path : /home
    - description : provides a button to remove authorization


3. SynchronizationComponent :
    - path : /synchronization
    - description : a page to show the list of users synchronized thanks to the oauth connection to Emalsys


4. RostersSelectionComponent :
    - path : /rosters/selection
    - description : a page to select the rosters to synchronize
