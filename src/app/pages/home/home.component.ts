import { Component, OnInit  } from '@angular/core';

import { CacheService       } from '../../services/cache.service';
import { FosOAuth2Service   } from '../../services/fos-oauth2.service';

import { Router, ActivatedRoute, Params} from '@angular/router';

import { Parameters         } from '../../app.parameters'
import { Token              } from '../../classes/model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    token:Token;

    constructor(
        public cache:CacheService,
        public oauth:FosOAuth2Service,
        private router : Router,
    ) { }

    ngOnInit(){
        this.token = this.cache.get('access_token');
    }

    disconnect(){
        this.router.navigate(['/login']);
    }

    removeAuthorization(){
        this.cache.clear();
        window.location.reload()
    }



}
