import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RostersSelectionComponent } from './rosters-selection.component';

describe('RostersSelectionComponent', () => {
  let component: RostersSelectionComponent;
  let fixture: ComponentFixture<RostersSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RostersSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RostersSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
