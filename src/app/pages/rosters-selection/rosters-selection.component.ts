import { Component, OnInit  } from '@angular/core';

import { CacheService       } from '../../services/cache.service';
import { FosOAuth2Service   } from '../../services/fos-oauth2.service';

import { Router, ActivatedRoute, Params} from '@angular/router';

import { Parameters         } from '../../app.parameters';
import { Token              } from '../../classes/model';

@Component({
    selector: 'app-rosters-selection',
    templateUrl: './rosters-selection.component.html',
    styleUrls: ['./rosters-selection.component.css']
})
export class RostersSelectionComponent implements OnInit {

    operationStatus = '';
    rosters = [];
    synchronizedRosters = [];
    token:Token;

    constructor(
        public cache:CacheService,
        public oauth:FosOAuth2Service,
        private router : Router,
    ) { }

    ngOnInit() {
        //retrieve the last token saved
        this.token = this.cache.get('access_token');
        this.synchronizedRosters = this.cache.get('synchronizedRosters') || [];

        if(this.token){

            this.oauth.getRosters(this.token).subscribe( response =>{

                this.rosters = response.json();

            }, error => {

                this.operationStatus = "Failed getting rosters";

                //force authorization
                this.cache.set('access_token', null);
                window.location.reload();

            })

        }else{
            // YOU CAN FORCE AUTHORIZATION
            this.oauth.openAuthorizationWindow(Parameters.get('client_id'),Parameters.get('redirect_uri'), Parameters.get('default_scope'));
        }
    }

    leave(){
        this.router.navigate(['/synchronization'], {queryParams:{reload:true}});
        window.location.reload();
    }

    synchronizeRoster(roster_id:number){
        if( this.synchronizedRosters.find(item => item == roster_id) === undefined ){
            this.synchronizedRosters.push(roster_id);
            this.cache.set('synchronizedRosters', this.synchronizedRosters);
        }
        this.operationStatus = "Roster "+roster_id+" is now synchronized";
    }

    isSynchronizedRoster(roster_id){
        return this.synchronizedRosters.find(item => item == roster_id) !== undefined;
    }

    unSynchronize(roster_id){
        let i = this.synchronizedRosters.findIndex(item => item == roster_id);
        if(i >= 0){
            this.synchronizedRosters.splice(i,1);
            this.cache.set('synchronizedRosters', this.synchronizedRosters);
        }
    }

    synchronizeAll(){
        this.synchronizedRosters = [];
        this.rosters.forEach( item => {
            this.synchronizedRosters.push(item.id)
        });

        this.operationStatus = "All your rosters are now synchronized";
        this.cache.set('synchronizedRosters', this.synchronizedRosters);
    }

    unSynchronizeAll(){
        this.synchronizedRosters = [];
        this.cache.set('synchronizedRosters', this.synchronizedRosters);
    }

}
