import { Component, OnInit  } from '@angular/core';

import { CacheService       } from '../../services/cache.service';
import { FosOAuth2Service   } from '../../services/fos-oauth2.service';

import { Router, ActivatedRoute, Params} from '@angular/router';

import { Parameters         } from '../../app.parameters';
import { Token              } from '../../classes/model';


@Component({
    selector: 'app-synchronization',
    templateUrl: './synchronization.component.html',
    styleUrls: ['./synchronization.component.scss']
})
export class SynchronizationComponent implements OnInit {

    token:Token;
    users = [];
    operationStatus = '';
    updateTime = null;
    userDetailsToggled = {};

    constructor(
        public cache:CacheService,
        public oauth:FosOAuth2Service,
        private router : Router,
        private route : ActivatedRoute,
    ) {
        this.updateTime = (new Date()).getTime();
    }

    ngOnInit() {
        //retrieve the last token saved or create an empty token
        this.token = this.cache.get('access_token') || new Token();

        //list of users or empty array
        this.users = this.cache.get('users') || [];

        //if the caller (rosters-selection-component) added a parameter '?reload=true' then we auto-synchronize
        this.route.queryParams.subscribe( params => {
            if(params.reload){
                this.synchronize()
            }
        })
    }

    synchronize(){
        //empty the users array
        this.users = [];

        //get the list of synchronized rosters
        let synchronizedRosters = this.cache.get('synchronizedRosters') || [];

        if(synchronizedRosters.length){

            //we dont use forEach to prevent asynchronous calls
            for(let roster_id of synchronizedRosters){

                this.oauth.getRosterMembers(this.token, roster_id)
                .toPromise().then( async response => {

                    let members = response.json();

                    //avoid double. we only add user which are not in the list
                    members.forEach( (m,i) => {
                        if(this.users.findIndex(u => u.user.id == m.user.id) < 0){
                            this.users.push(members[i])
                        }
                    });

                    //reset the cache
                    this.cache.set('users', this.users);

                }).catch(error => {
                    // this.operationStatus = "Failed to get roster members";
                    // YOU CAN FORCE AUTHORIZATION
                    this.oauth.openAuthorizationWindow(Parameters.get('client_id'),Parameters.get('redirect_uri'), Parameters.get('default_scope'));
                });
            }

        }else{
            //if there is any rosters then the user is redirected to the roster selection
            this.router.navigate(['/rosters/selection']);            
        }
    }

    /**
     * Toggle the details view when you click on 'details'
     * @param  number user_id
     * @return void
     */
    toggleUserDetails(user_id){
        this.userDetailsToggled[user_id] = ! this.userDetailsToggled[user_id]
    }

    /**
     * Checks if 'details' section is shown
     * @param  number user_id
     * @return void
     */
    isToggledUserDetails(user_id){
        return this.userDetailsToggled[user_id] == true;
    }

}
