import { Component, OnInit  } from '@angular/core';

import { Router, ActivatedRoute, Params} from '@angular/router';

import { CacheService       } from '../../services/cache.service';
import { FosOAuth2Service   } from '../../services/fos-oauth2.service';

import { Parameters         } from '../../app.parameters';
import { Token              } from '../../classes/model';


@Component({
  selector: 'app-accept-token',
  templateUrl: './accept-token.component.html',
  styleUrls: ['./accept-token.component.css']
})
export class AcceptTokenComponent implements OnInit {

  constructor(
      private activatedRoute : ActivatedRoute,
      private router : Router,
      private oauth : FosOAuth2Service,
      private cache : CacheService,
  ) { }

  ngOnInit() {

      //this page matches with the redirect_uri
      //after authorization by the oauth server, we are redirected here
      this.activatedRoute.queryParams.subscribe( (params ) => {

          if(params.code){

              let uri = '?grant_type=authorization_code&code='+params.code+'&client_id='+Parameters.get('client_id')
                  +'&client_secret='+Parameters.get('client_secret')
                  +'&redirect_uri='+Parameters.get('redirect_uri')+'/accepttoken';

              this.oauth.getAccessToken(uri).then((response:any) => {

                  let token:Token = response.json() as Token;
                  this.cache.set('access_token', token);

                  //if the token is valid then go to the rosters selection
                  opener.location = "/rosters/selection";

                  //This component will be openned inside the popup. That's why we need to use self.close()
                  self.close();

              }).catch(error=>{
                  this.cache.set('access_token', null);
              })
          }else{
              //if there's any supplied code then go back to the synchronization
              opener.location = "/synchronization";

              //This component will be openned inside the popup. That's why we need to use self.close()
              self.close();
          }
      })
  }

}
