import { Component, OnInit      } from '@angular/core';
import { Router                 } from '@angular/router';

import { CacheService           } from '../../services/cache.service';
import { FosOAuth2Service       } from '../../services/fos-oauth2.service';

import { Parameters             } from '../../app.parameters'
import { User, Token            } from '../../classes/model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user:User;

    constructor(
        public cache:CacheService,
        public oauth:FosOAuth2Service,
        public router:Router,
    ) {
        this.user = new User();
    }

    ngOnInit() {
    }

    /**
     * An example to show how to refresh the token when the user login himself to the Alert Platform
     */
    async login(){
        //your own treatments
        //...


        //suppose login successful on Alerte Platform
        //then check for access token
        //we will assume that the localStorage is the database of Alerte Platform
        let token:Token = this.cache.get('access_token');

        if(token){
            // if token exists then let refresh it
            let _body = new FormData();
            _body.append('grant_type',      'refresh_token');
            _body.append('client_id',       Parameters.get('client_id'));
            _body.append('client_secret',   Parameters.get('client_secret'));
            _body.append('redirect_uri',    Parameters.get('redirect_uri'));
            _body.append('refresh_token',   token.refresh_token);

            await this.oauth.refreshToken(_body).then( (response : any) => {

                token = response.json() as Token;

                //update access_token in the localStorage
                this.cache.set('access_token',token);

                //for the demo, we redirect to the page with the list of synchronized users if everything is ok
                this.router.navigate(['/synchronization'])

            }).catch( error => {

                this.cache.set('access_token',null);

                // YOU CAN FORCE AUTHORIZATION
                this.oauth.openAuthorizationWindow(Parameters.get('client_id'),Parameters.get('redirect_uri'), Parameters.get('default_scope'));
            })
        }else{
            //if the token is not defined, ignore the access token step
            this.router.navigate(['/synchronization'])
        }


    }



}
