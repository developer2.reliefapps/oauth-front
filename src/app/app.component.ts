import { Component          } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Http, Headers, RequestOptions, Request, RequestMethod} from '@angular/http';

import { FosOAuth2Service   } from './services/fos-oauth2.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title: string = 'Alerte Platform';
}
