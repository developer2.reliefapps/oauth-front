import { Injectable         } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Request, RequestMethod } from '@angular/http';

/**
 * A service to handle api calls and oauth accesses
 */
@Injectable()
export class FosOAuth2Service {

    readonly url        = "http://emalsys.dev/app_dev.php/api/oauth/v3";
    readonly shortUrl   = "http://emalsys.dev/app_dev.php";

    constructor(
        private http:Http
    ){}

    /**
     * Build the headers map by adding the authorization
     * @param  Token token
     * @return object {
     *         headers :  a map of predefined headers
     * }
     */
    getHeaders(token){
        let headers = new Headers();
        headers.append('Authorization', "Bearer "+token.access_token)
        return {headers:headers};
    }

    /**
     * Api call to get the list of organizations
     * @param  Token token
     * @return Observable<Response>
     */
    getOrganizations(token){
        let url = this.url+"/organisations";
        let headers = this.getHeaders(token);

        return this.http.get(encodeURI(url), headers);
    }

    /**
     * Api call to get the list of global rosters
     * @param  Token token
     * @return Observable<Response>
     */
    getRosters(token){
        let url = this.url+"/rosters";
        let uri = '?method=by_organisation&filter=joinable';  // default
        // let uri = '?method=by_organisation&id=1';  // if you want a custom organization
        let headers = this.getHeaders(token);

        return this.http.get(encodeURI(url+uri), headers);
    }

    /**
     * Api call to get the list of roster's members
     * @param  Token token
     * @param  number roster_id
     * @return Observable<Response>
     */
    getRosterMembers(token, roster_id){
        let url = this.url+"/rosters/"+roster_id;
        let uri = '?filter=members';
        let headers = this.getHeaders(token);

        return this.http.get(encodeURI(url+uri), headers);
    }

    /**
     * Api call to get the user profile
     * @param  Token token
     * @param  number user_id
     * @return Observable<Response>
     */
    getUserProfile(token, user_id){
        let url = encodeURI(this.url+"/users/"+user_id);
        let headers = this.getHeaders(token);

        return this.http.get(url, headers);
    }

    /**
     *
     * @param  string usingUri='' A uri string containing the parameters required to request a token
     * Ex :
     * '?grant_type=authorization_code   // assume we are using this grant_type
     *   &code=<CODE>
     *   &client_id=<CLIENT_ID>
     *   &client_secret=<CLIENT_SECRET>
     *   &redirect_uri=<REDIRECT_URI>;
     * @return Observable<Response>
     */
    async getAccessToken(usingUri=''){
        let url = encodeURI(this.shortUrl+"/oauth/v2/token"+usingUri);

        return await new Promise((resolve, reject)=>{
            this.http.get(url).subscribe( response => resolve(response), error => reject(error))
        })
    }

    /**
     * Refresh token
     * The refresh token url requires to post a form that's why we are using a FormData
     * @param  {FormData} formData {
     *     grant_type : 'refresh_token'
     *     client_id : the client id
     *     client_secret : the client secret
     *     redirect_uri : the redirect uri
     *     refresh_token : the current refresh token
     * }
     * @return Observable<Response>
     */
    async refreshToken(formData:FormData){
        let url = encodeURI(this.shortUrl+"/oauth/v2/token");

        return await new Promise((resolve, reject)=>{
            this.http.post(url, formData).subscribe( response => resolve(response), error => reject(error))
        })
    }



    /**
     * Build the url for accessing the authorization page.
     * We assume that the authorization_code grant type is used.
     * @param  {any}    parameters {
     *     client_id : the client id
     *     redirect_uri : the client redirection url
     *     response_type : the response_type. we assume that we are using the 'authorization_code' grant type
     *     scope : the scope to access. not used for now . the default value shoud be 'rosters'
     * }
     * @return string
     */
    getAuthorizeUrl(parameters:any){
        let url = this.shortUrl+"/oauth/v2/auth";
        let uri = '?client_id='+parameters.client_id+'&redirect_uri='+parameters.redirect_uri+'&response_type=code&scope='+parameters.scope;
        return encodeURI(url+uri);
    }

    /**
     * Open a window for authorization
     * @param  string clientId
     * @param  string redirectUri
     * @param  string scope       The scope. 'rosters' can be used in the current version.
     * @return void
     */
    openAuthorizationWindow(clientId, redirectUri, scope ){
        // by building the url to request an authorization form
        let url = this.getAuthorizeUrl({
            'client_id':    clientId,
            'redirect_uri': redirectUri,
            'scope':        scope
        });

        window.open(url,'popup','width=500,height=500,scrollbars=no,resizable=no');
    }

    // ( Dont REMOVE this archive )
    // async getTokenWorking( access, auth_code = ''){
    //     let url = encodeURI(this.url+"/oauth/v2/token");
    //
    //     // let url = "http://emalsys.dev/app_dev.php/oauth/token";
    //     // url = encodeURI(url);
    //
    //     let headers = new Headers();
    //     // headers.append('Authorization', "Bearer "+access.authorization)
    //     // headers.append('Authorization', "Basic "+btoa(access.client+':'+access.secret))
    //
    //     // let uri = '?grant_type='+access.grant_type+'&client_id='+access.client+'&client_secret='+access.secret+'&redirect_uri='+access.redirectUri
    //     let body:any = {
    //         grant_type: access.grant_type, // 'authorization_code',
    //         // code: auth_code,
    //         client_id: access.client,
    //         client_secret: access.secret,
    //         redirect_uri: access.redirectUri,
    //         username: access.username,
    //         password: access.password
    //     };
    //
    //     // https://stackoverflow.com/questions/40436958/fosoauthserverbundle-invalid-grant-type-parameter-or-parameter-missing
    //     let _body = new FormData();
    //     _body.append('grant_type',  access.grant_type);
    //     _body.append('client_id',  access.client);
    //     _body.append('client_secret',  access.secret);
    //     _body.append('redirect_uri',  access.redirectUri);
    //     _body.append('username',  access.username);
    //     _body.append('password',  access.password);
    //     return await new Promise((resolve, reject)=>{
    //         this.http.post(url, _body, {headers:headers}).subscribe( response => resolve(response), error => reject(error))
    //     })
    // }

}
