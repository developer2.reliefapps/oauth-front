import { Injectable         } from '@angular/core';

/**
 * Cache service.
 */
@Injectable()
export class CacheService{

    readonly prefix = 'Alerte_';

    get(key:string){
        let value = localStorage.getItem(this.prefix+key);
        return value ? JSON.parse(value) : null;
    }

    set(key:string, object:any){
        localStorage.setItem(this.prefix+key,JSON.stringify(object));
    }

    remove(key:string){
        localStorage.removeItem(this.prefix+key);
    }

    clear(){
        //we dont use localStorage.clear() because we dont want to delete all the storage since others applications are working on the same browser
        let keys = Object.keys(localStorage);
        keys.forEach( (key) => {
            if(key.match(this.prefix)){
                localStorage.removeItem(key);
            }
        })
    }


}
