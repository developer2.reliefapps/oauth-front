
export class Token{
    access_token:string ; //"YTIyNWE2YzIyODMxMzlkZjk4MzYxNzJiNjY2MTM0Mjg1NWEzY2UxNDYwYTNmZTNhNDZmMDA1MGU0YWNiNjk3ZQ"
    expires_in:number; //84600
    refresh_token:string; //"YzZjZmFiMjFjZGVkYjBjMTFkZTIzNWNkODBjYTA1M2IwM2QzMjU3OGRiNWE0YWFhZTVjZGQ5ZDNkOGRmNzA3ZA"
    scope:string; //"rosters"
    token_type:string; //"bearer"
}

export class User {
    login:string                = 'user';
    password:string             = 'user';
    emalsysAccessToken:string   = '';
    emalsysAuthCode:string      = '';
    emalsysRefreshToken:string  = '';
}
