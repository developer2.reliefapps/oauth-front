import { BrowserModule      } from '@angular/platform-browser';
import { NgModule           } from '@angular/core';
import { ActivatedRoute, RouterModule }  from '@angular/router';
import { HttpModule         }  from '@angular/http';
import { FormsModule        } from '@angular/forms';

import { FosOAuth2Service   } from './services/fos-oauth2.service';
import { CacheService       } from './services/cache.service';


import { AppComponent       } from './app.component';
import { LoginComponent     } from './pages/login/login.component';
import { HomeComponent      } from './pages/home/home.component';
import { AcceptTokenComponent } from './pages/accept-token/accept-token.component';
import { SynchronizationComponent } from './pages/synchronization/synchronization.component';
import { RostersSelectionComponent } from './pages/rosters-selection/rosters-selection.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AcceptTokenComponent,
    SynchronizationComponent,
    RostersSelectionComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
        { path: 'login',            component: LoginComponent },
        { path: 'home',             component: HomeComponent },
        { path: 'accepttoken',      component: AcceptTokenComponent },
        { path: 'synchronization',  component: SynchronizationComponent },
        { path: 'rosters/selection', component: RostersSelectionComponent },
        { path : '**',              component: SynchronizationComponent }
    ]),
    HttpModule,
    FormsModule
  ],
  providers: [
      FosOAuth2Service,
      CacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
